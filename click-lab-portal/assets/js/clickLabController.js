angular.module('clickLabApp', []).controller('clickLabCtrl', function ($scope,
                                                                       $window) {

    $scope.loading = false;
    $scope.cookiesPopUp = true;

    /*
    *@Desc Hide the cookies popup.
    */
    $scope.agreeCokies = function(){
            $scope.cookiesPopUp = false;
    }
    /**
    * @Param formdData {object} The data from the form, name and message can be null;
    * @Desc Upload the data from the form. 
    */
    $scope.uploadFormData = function (formdData) {
        $scope.loading = true;
    
        if(formdData.formName === undefined || formdData.formName === "" ){
            formdData.formName = "Name not specific"
        }
        if(formdData.formMessage === undefined || formdData.formMessage === ""){
            formdData.formMessage = "Message not specific"
        }
        console.log(formdData);
        $http.post('localhost:8080/contact', formdData)
            .then(function (succes) {
                $scope.loading = false;
                window.location.reload();
            })
            .catch(function (error) {
            })
    }
});